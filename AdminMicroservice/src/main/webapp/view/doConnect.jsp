<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<title>Welcome to DoConnect</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
</head>

<style>
img {
  display: block;
  max-width: 100%;
  height: auto;
}
</style>

<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<div class="navbar-collapse collapse">
				<div class="page-header">
					<center>
						<h1 style="color: white;">DoConnect</h1>
					</center>
				</div>
			</div>
		</div>
	</div>

  <img src="assets/img/slide/book1.jpg" class="img-fluid" alt="Responsive image" max-width: 100%;>
		<div class="carousel-container">
			<center>
				<div class="carousel-content">
					<h2 class="animate__animated animate__fadeInDown">Admin
						Login/Registration</h2>
					<p class="animate__animated animate__fadeInUp">DoConnect Admin MicroService
						</p>
					<h1>
						<a href="/home"
							class="btn-get-started animate__animated animate__fadeInUp scrollto">Admin</a>
					</h1>
				</div>
			</center>
		</div>
	</div>

	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>