package com.app.UserMicroservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.UserMicroservice.model.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> 
{
	//Queries
	@Query("Select qobj from Question qobj where qobj.isApproved = false")
	public List<Question> findByIsApproved();
	
	@Query("Select qobj from Question qobj where qobj.isApproved = true")
	public List<Question> findByIsApprovedQuestions();
	
	@Query("Select qobj from Question qobj")
	public List<Question> findByQuestions();
	
	@Query("Select qobj from Question qobj where qobj.question=?1 and qobj.isApproved = true")
	public List<Question> searchFunctionality(String question);

	@Query("Select qobj from Question qobj where qobj.questionTopic=?1 and qobj.isApproved = true")
	public List<Question> searchTopic(String questionTopic);
	
	Question findByQuestionId(int questionId);

}
