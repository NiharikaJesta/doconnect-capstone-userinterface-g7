package com.app.UserMicroservice.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.app.UserMicroservice.configuration.JwtTokenUtil;
import com.app.UserMicroservice.dto.UserDTO;
import com.app.UserMicroservice.model.JwtResponse;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.UserRepository;
import com.app.UserMicroservice.service.JwtUserDetailsService;
import com.app.UserMicroservice.service.UserService;

@RestController
@CrossOrigin
public class JwtAuthenticationController
{

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;

	//Validation Layer(Authentication)
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(
			@ModelAttribute("user") UserDTO authenticationRequest,HttpSession session) throws Exception 
	{
		System.out.println("username is :" + authenticationRequest.getUsername());
		System.out.println("userId is :" + authenticationRequest.getUserId());
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);
		System.out.println("Token displaying.......");
		System.out.println("Token is " + token);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	//Registering data storing
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") UserDTO user, HttpServletRequest request,HttpSession session) throws Exception
	{
		if (userService.findByUsername(user.getUsername()) != null) 
		{
			request.setAttribute("error", "Username already Exist");
			request.setAttribute("mode", "MODE_REGISTER");
			return new ModelAndView("welcomepage");
		} 
		else
		{
			
			userDetailsService.save(user);
			request.setAttribute("mode", "MODE_HOME");
			return new ModelAndView("welcomepage");
		}
	}

	//Validating the Credentials
	private void authenticate(String username, String password) throws Exception 
	{
		try 
		{
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} 
		catch (DisabledException e)
		{
			throw new Exception("USER_DISABLED", e);
		} 
		catch (BadCredentialsException e)
		{
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
