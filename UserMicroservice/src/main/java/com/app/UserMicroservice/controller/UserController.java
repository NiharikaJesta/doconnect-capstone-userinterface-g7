package com.app.UserMicroservice.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.UserMicroservice.dto.UserDTO;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.AnswerRepository;
import com.app.UserMicroservice.repository.QuestionRepository;
import com.app.UserMicroservice.repository.UserRepository;
import com.app.UserMicroservice.service.AnswerService;
import com.app.UserMicroservice.service.QuestionService;
import com.app.UserMicroservice.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin()
public class UserController 
{

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserService userService;

	@Autowired
	QuestionService questionService;

	@Autowired
	AnswerService answerService;

	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	AnswerRepository answerRepository;

	// Swagger
	@ApiOperation(value = "User List", notes = "This method will retrieve list from database", nickname = "getUser")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "User not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = UserData.class, responseContainer = "List") })

	// Here, Web page starts
	@RequestMapping("/") 
	public ModelAndView Welcome() 
	{
		return new ModelAndView("doConnect");
	}

	// UserData Home
	@RequestMapping("/welcome")
	public ModelAndView welcome(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_HOME");
		return new ModelAndView("welcomepage");
	}
	
	// User operations Home
	@RequestMapping("/adminHome")
	public ModelAndView home(HttpServletRequest request)
	{
		request.setAttribute("mode", "USER_HOME");
		return new ModelAndView("userWelcomePage");
	}

	// User Register
	@RequestMapping("/UserDataRegister")
	public ModelAndView insertForm(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_REGISTER");
		return new ModelAndView("welcomepage", "user", new UserDTO());
	}

	// User Login
	@RequestMapping("/login")
	public ModelAndView loginForm(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_LOGIN");
		return new ModelAndView("welcomepage", "user", new UserDTO());
	}

	// Logout
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) 
	{
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "redirect:/";
	}

	// CRUD Operations on User
	@RequestMapping({ "/data" }) 
	public ModelAndView showAllUsers(HttpServletRequest request) 
	{
		List<UserData> ulist = userService.showAllUsers();
		request.setAttribute("mode", "ALL_USERS");
		return new ModelAndView("adminOperations", "userdata", ulist);
	}

	@RequestMapping("/update/{userId}")
	public ModelAndView editUser(@PathVariable("userId") int userId, HttpServletRequest request) 
	{
		request.setAttribute("user", userService.editUser(userId));
		request.setAttribute("mode", "MODE_UPDATE");
		return new ModelAndView("adminOperations");
	}
	
	@RequestMapping("/saveUpdation/{user}")
	public ModelAndView saveUpdates(@ModelAttribute UserData user) 
	{
		userRepository.save(user);
		return new ModelAndView("adminOperations");
	}

	@RequestMapping("/delete/{userId}")
	public ModelAndView deleteUser(@PathVariable("userId") int userId, HttpServletRequest request) 
	{
		userService.deleteMyUser(userId);
		List<UserData> ulist = userService.showAllUsers();
		request.setAttribute("mode", "ALL_USERS");
		return new ModelAndView("adminOperations", "userdata", ulist);
	}
}
