package com.app.UserMicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.UserMicroservice.dto.PostAnswer;
import com.app.UserMicroservice.model.Answer;
import com.app.UserMicroservice.model.Question;
import com.app.UserMicroservice.model.UserData;
import com.app.UserMicroservice.repository.AnswerRepository;
import com.app.UserMicroservice.repository.QuestionRepository;
import com.app.UserMicroservice.repository.UserRepository;

@Service
public class AnswerService 
{
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private AnswerRepository answerRepository;

	//PostAnswer Operation Business Logic
	public Answer giveAnswer(PostAnswer postAnswer) 
	{
		Answer answer = new Answer();
		Optional<UserData> user = userRepository.findById(postAnswer.getUserId());
		Optional<Question> question = questionRepository.findById(postAnswer.getQuestionId());
		answer.setQuestion(question.get());
		answer.setAnswer(postAnswer.getAnswer());
		answer.setIsApproved(false);
		answer.setAnswerUser(user.get());
		answerRepository.save(answer);
		return answer;
	}

	public List<Answer> getAnswers(int questionId) 
	{
		return answerRepository.getAnswer(questionId);
	}

	public List<Answer> getUnApprovedAnswers()
	{
		return answerRepository.findByIsApproved();
	}

	public List<Answer> getApprovedAnswers() 
	{
		return answerRepository.findByIsApprovedAnswers();
	}

	//Approve Operation
	public Answer approveAnswer(int answerId)
	{
		Answer answer = answerRepository.findByAnswerId(answerId);
		answer.setIsApproved(true);
		answer = answerRepository.save(answer);
		return answer;
	}

	//Retrieve Operation
	public List<Answer> showAllAnswers() 
	{
		List<Answer> answers = new ArrayList<Answer>();
		for (Answer answer : answerRepository.findAll()) {
			answers.add(answer);
		}

		return answers;
	}

	//Delete Operation
	public void deleteAnswer(int answerId) 
	{
		answerRepository.deleteById(answerId);
	}
}
