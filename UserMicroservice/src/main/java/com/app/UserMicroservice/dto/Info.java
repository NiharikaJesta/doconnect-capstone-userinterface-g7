package com.app.UserMicroservice.dto;

import com.app.UserMicroservice.model.Answer;
import com.app.UserMicroservice.model.Question;
import com.app.UserMicroservice.model.UserData;

public class Info 
{
	private Admin admin;
	private UserData userData;
	private Answer answer;
	private Question question;

	public Admin getAdmin() 
	{
		return admin;
	}

	public void setAdmin(Admin admin) 
	{
		this.admin = admin;
	}

	public UserData getUserData() 
	{
		return userData;
	}

	public void setUserData(UserData userData)
	{
		this.userData = userData;
	}

	public Answer getAnswer()
	{
		return answer;
	}

	public void setAnswer(Answer answer)
	{
		this.answer = answer;
	}

	public Question getQuestion()
	{
		return question;
	}

	public void setQuestion(Question question) 
	{
		this.question = question;
	}

}
