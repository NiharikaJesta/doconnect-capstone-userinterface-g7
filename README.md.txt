 
Project Name:DoConnect
Problem Statement:

DoConnect is a popular Q and A form in which techniques questions were asked and answered.
There are 2 users in this application:
1. User
2. Admin


##User Stories:
1. As a user, I should be able to login, log out and register into the application.
2. As a User, I should be able to ask any question under any topic
3. As a User, I should be able to search the question on any string written in search box
4. As a User, I should be able to answer any question asked
5. As a User, I should be able to answer more that one question and more than one time
6. As a User, I should be able to chat with other users


##Admin Stories:
1. As an Admin, I should be able to log in, logout, and register into the application
2. As an admin, I should be able to approve the question and answer. Any question or answer 
will be visible on platform only if it is approved
3. As an admin, I should be able to delete inappropriate Questions or Answers.


##Instructions:
1. Use JWT
2. Please share the database structure in the .sql file
3. Please create a separate microservice for delete inappropriate Questions or Answers
